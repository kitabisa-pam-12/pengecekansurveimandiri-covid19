package stud11418042.develops.groupproject.util;

import android.app.Activity;
import android.content.Intent;

import stud11418042.develops.groupproject.AppConstants;
import stud11418042.develops.groupproject.model.Note;
import stud11418042.develops.groupproject.ui.activity.AddNoteActivity;
import stud11418042.develops.groupproject.ui.activity.PwdActivity;

public class NavigatorUtils implements AppConstants {

    public static void redirectToPwdScreen(Activity activity,
                                           Note note) {
        Intent intent = new Intent(activity, PwdActivity.class);
        intent.putExtra(INTENT_TASK, note);
        activity.startActivityForResult(intent, ACTIVITY_REQUEST_CODE);
    }


    public static void redirectToEditTaskScreen(Activity activity,
                                                Note note) {
        Intent intent = new Intent(activity, AddNoteActivity.class);
        intent.putExtra(INTENT_TASK, note);
        activity.startActivityForResult(intent, ACTIVITY_REQUEST_CODE);
    }

    public static void redirectToViewNoteScreen(Activity activity,
                                                Note note) {
        Intent intent = new Intent(activity, AddNoteActivity.class);
        intent.putExtra(INTENT_TASK, note);
        intent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
        activity.startActivity(intent);
        activity.finish();
    }
}
