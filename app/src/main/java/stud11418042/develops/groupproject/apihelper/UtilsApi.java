package stud11418042.develops.groupproject.apihelper;

public class UtilsApi {
    public static final String BASE_URL_API = "http://localhost:8080/PAM/ProyekKelompok/Mahasiswa/";

    // Mendeklarasikan Interface BaseApiService
    public static BaseApiDervice getAPIService(){
        return RetrofitClient.getClient(BASE_URL_API).create(BaseApiDervice.class);
    }

}
